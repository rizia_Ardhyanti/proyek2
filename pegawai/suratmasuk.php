<?php include 'header.php'; ?>

<div class="breadcome-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcome-list">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="breadcome-heading">
                                <h4 style="margin-bottom: 0px">Surat Masuk</h4>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <ul class="breadcome-menu" style="padding-top: 0px">
                                <li><a href="#">Home</a> <span class="bread-slash">/</span></li>
                                <li><span class="bread-blod">Surat Masuk</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="panel panel">

        <div class="panel-heading">
            <h3 class="panel-title">Surat Masuk</h3>
        </div>
        <div class="panel-body">

            <table id="table" class="table table-bordered table-striped table-hover table-datatable">
                <thead>
                    <tr>
                        <th width="1%">No</th>
                        <th>No. Agenda <br> Kode</th>
                        <th>Isi Ringkasan <br> File</th>
                        <th>Asal Surat</th>
                        <th>No. Surat <br> Tgl. Surat</th>
                        <th class="text-center" width="21%">Tindakan</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                    include '../koneksi.php';
                    $no = 1;
                    $arsip = mysqli_query($koneksi,"SELECT * FROM surat_masuk,admin WHERE id_admin=admin_id ORDER BY id_surat DESC");
                    while($p = mysqli_fetch_array($arsip)){
                        ?>
                        <tr>
                            <td><?php echo $no++; ?></td>
                            <td>
                                <b>Agenda</b> : <?php echo $p['no_agenda'] ?><br>
                                <hr>
                                <b>Kode</b> : <?php echo $p['kode'] ?><br>
                            </td>

                             <td>
                                <b>Isi</b> : <?php echo $p['isi'] ?><br>
                                <hr>
                                <b>File</b> : <?php echo $p['file'] ?><br>
                            </td>

                            <td><?php echo $p['asal_surat'] ?></td>

                            <td>
                                <b>No</b> : <?php echo $p['no_surat'] ?><br>
                                <hr>
                                <b>Tgl.</b> : <?php echo date('d-m-Y',strtotime($p['tgl_surat'])) ?></td>
                            </td>

                            <td class="text-center">


                                <div class="modal fade" id="exampleModal_<?php echo $p['id_surat']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">PERINGATAN!</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                Apakah anda yakin ingin menghapus data ini? <br>file dan semua yang berhubungan akan dihapus secara permanen.
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batalkan</button>
                                                <a href="suratmasuk_hapus.php?id=<?php echo $p['id_surat']; ?>" class="btn btn-primary"><i class="fa fa-check"></i> &nbsp; Ya, hapus</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="btn-group">
                                    <a href="disposisi.php?id=<?php echo $p['id_surat']; ?>" class="btn btn-default"><i class="fa fa-upload"></i> Dis</a>
                                    <a target="_blank" class="btn btn-default" href="../arsip/<?php echo $p['file']; ?>"><i class="fa fa-download"></i></a>
                                    <a target="_blank" href="suratmasuk_preview.php?id=<?php echo $p['id_surat']; ?>" class="btn btn-default"><i class="fa fa-search"></i> Preview</a>
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal_<?php echo $p['id_surat']; ?>">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </div>
                            </td>
                        </tr>
                        <?php 
                    }
                    ?>
                </tbody>
            </table>


        </div>

    </div>
</div>


<?php include 'footer.php'; ?>