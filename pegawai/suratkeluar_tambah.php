<?php include 'header.php'; ?>

<div class="breadcome-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcome-list">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="breadcome-heading">
                                <h4 style="margin-bottom: 0px">Upload Surat Keluar</h4>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <ul class="breadcome-menu" style="padding-top: 0px">
                                <li><a href="#">Home</a> <span class="bread-slash">/</span></li>
                                <li><span class="bread-blod">Surat Keluar</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">


    <div class="row">
        <div class="col-lg-6 col-lg-offset-3">
            <div class="panel panel">

                <div class="panel-heading">
                    <h3 class="panel-title">Upload Surat Keluar</h3>
                </div>
                <div class="panel-body">

                    <div class="pull-right">            
                        <a href="suratkeluar.php" class="btn btn-sm btn-primary"><i class="fa fa-arrow-left"></i> Kembali</a>
                    </div>

                    <br>
                    <br>

                    <form method="post" action="suratkeluar_aksi.php" enctype="multipart/form-data">
                            <input type="hidden" class="form-control" name="status">
                            <input type="hidden" class="form-control" name="no_surat">
                            
                            <div class="form-group">
                                <label>No. Agenda</label>
                                <input type="text" class="form-control" name="no_agenda" required="required">
                            </div>

                            <div class="form-group">
                                <label>Tujuan</label>
                                <input type="text" class="form-control" name="tujuan" required="required" >
                            </div>

                            <div class="form-group">
                                <label>Isi Ringkasan</label>
                                <textarea class="form-control" name="isi" required="required"></textarea>
                            </div>

                            <div class="form-group">
                                <label>Kode</label>
                                <input type="text" class="form-control" name="kode" required="required">
                            </div>

                            <div class="form-group">
                                <label>Tanggal Surat</label>
                                <input type="date" class="form-control" name="tgl_surat" required="required">
                            </div>

                            <div class="form-group">
                                <label>File</label>
                                <input type="file" name="file">
                            </div>

                            <div class="form-group">
                                <label>Keterangan</label>
                                <textarea class="form-control" name="keterangan" required="required"></textarea>
                            </div>

                            <div class="form-group">
                            <label>Status</label>
                            <select class="form-control" id="status" name="status" style="width: 100%;">
                                <option value=" ">-- Pilih Status --</option>
                                <option>Belum Dikirim</option>
                                <option>Dikirim</option>
                                <option>Tidak Dikirm</option>
                              </select>
                        </div>

                        <div class="form-group">
                            <label></label>
                            <input type="submit" class="btn btn-primary" value="Upload">
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>


</div>


<?php include 'footer.php'; ?>