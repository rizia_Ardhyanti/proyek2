<?php include 'header.php'; ?>
<div class="breadcome-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcome-list">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="breadcome-heading">
                                <h4 style="margin-bottom: 0px">Preview Surat Masuk</h4>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <ul class="breadcome-menu" style="padding-top: 0px">
                                <li><a href="#">Home</a> <span class="bread-slash">/</span></li>
                                <li><span class="bread-blod">Preview</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">


    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel">

                <div class="panel-heading">
                    <h3 class="panel-title">Preview Surat Masuk</h3>
                </div>
                <div class="panel-body">

                    <a href="suratmasuk.php" class="btn btn-sm btn-primary"><i class="fa fa-arrow-left"></i> Kembali</a>

                    <br>
                    <br>

                    <?php 
                    $id = $_GET['id'];  
                    $data = mysqli_query($koneksi,"SELECT * FROM surat_masuk,admin WHERE id_admin=admin_id and id_surat='$id'");
                    while($d = mysqli_fetch_array($data)){
                        ?>

                        <div class="row">
                            <div class="col-lg-4">

                                <table class="table">
                                    <tr>
                                        <th>No. Agenda</th>
                                        <td><?php echo $d['no_agenda']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Asal Surat</th>
                                        <td><?php echo $d['asal_surat']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>No. Surat</th>
                                        <td><?php echo $d['no_surat']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Isi Ringkasan</th>
                                        <td><?php echo $d['isi']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Kode</th>
                                        <td><?php echo $d['kode']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Indeks</th>
                                        <td><?php echo $d['indeks']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Tanggal Surat</th>
                                        <td><?php echo $d['tgl_surat']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Waktu Upload</th>
                                        <td><?php echo date('d-m-Y',strtotime($d['tgl_catat'])) ?></td>
                                    </tr>
                                    <tr>
                                    <tr>
                                        <th>Jenis File</th>
                                        <td><?php echo $d['jenis']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Keterangan</th>
                                        <td><?php echo $d['keterangan']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Petugas Pengupload</th>
                                        <td><?php echo $d['admin_nama']; ?></td>
                                    </tr>
                                </table>

                            </div>
                            <div class="col-lg-8">

                                <?php 
                                if($d['jenis'] == "png" || $d['jenis'] == "jpg" || $d['jenis'] == "gif" || $d['jenis'] == "jpeg"){
                                    ?>
                                    <img src="../arsip/<?php echo $d['file']; ?>">
                                    
                                    <?php
                                }elseif($d['jenis'] == "pdf"){
                                    ?>

                                    <div class="pdf-singe-pro">
                                        <a class="media" href="../arsip/<?php echo $d['file']; ?>"></a>
                                    </div>

                                    <?php
                                }else{
                                    ?>
                                    <p>Preview tidak tersedia, silahkan <a target="_blank" style="color: blue" href="../arsip/<?php echo $d['file']; ?>">Download di sini.</a></p>.
                                    <?php
                                }
                                ?>

                            </div>
                        </div>







                        <?php 
                    }
                    ?>

                </div>
            </div>
        </div>
    </div>


</div>



<?php include 'footer.php'; ?>