/*
SQLyog Professional
MySQL - 10.1.38-MariaDB : Database - project_arsip_digital
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`project_arsip_digital` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `project_arsip_digital`;

/*Table structure for table `admin` */

DROP TABLE IF EXISTS `admin`;

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_nama` varchar(255) NOT NULL,
  `admin_nip` varchar(30) NOT NULL,
  `admin_bagian` varchar(30) NOT NULL,
  `admin_username` varchar(255) NOT NULL,
  `admin_password` varchar(255) NOT NULL,
  `admin_foto` varchar(255) NOT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `admin` */

insert  into `admin`(`admin_id`,`admin_nama`,`admin_nip`,`admin_bagian`,`admin_username`,`admin_password`,`admin_foto`) values 
(1,'Rizia Ardhyanti','12345678','Sekretariat','admin','c93ccd78b2076528346216b3b2f701e6','53567014_Picture11.png');

/*Table structure for table `arsip` */

DROP TABLE IF EXISTS `arsip`;

CREATE TABLE `arsip` (
  `arsip_id` int(11) NOT NULL AUTO_INCREMENT,
  `arsip_waktu_upload` datetime NOT NULL,
  `arsip_petugas` int(11) NOT NULL,
  `arsip_kode` varchar(255) NOT NULL,
  `arsip_nama` varchar(255) NOT NULL,
  `arsip_jenis` varchar(255) NOT NULL,
  `arsip_kategori` int(11) NOT NULL,
  `arsip_keterangan` text NOT NULL,
  `arsip_file` varchar(255) NOT NULL,
  PRIMARY KEY (`arsip_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Data for the table `arsip` */

insert  into `arsip`(`arsip_id`,`arsip_waktu_upload`,`arsip_petugas`,`arsip_kode`,`arsip_nama`,`arsip_jenis`,`arsip_kategori`,`arsip_keterangan`,`arsip_file`) values 
(2,'2019-10-10 15:09:59',4,'ARSIP-MN-0002','File keberngkatan','png',4,'tes ttead','1162363338_Screen Shot 2019-10-10 at 13.22.15.png'),
(3,'2019-10-10 16:02:44',4,'asd','asdasd 2x','pdf',3,'asdasd','432536246_mamunur.pdf'),
(4,'2019-10-12 17:02:16',5,'MN-002','Contoh Surat Izin Pelaksanaan','pdf',4,'Ini Contoh Surat Izin Pelaksanaan','1352467019_c4611_sample_explain.pdf'),
(5,'2019-10-12 17:03:01',5,'MN-003','Contoh Keputusan Kerja','pdf',3,'Contoh Keputusan Kerja pegawai','1765932248_Contoh-surat-lamaran-kerja-pdf (1).pdf'),
(6,'2019-10-12 17:03:37',5,'MN-004','Contoh Surat Izin Pegawai','pdf',7,'berikut Contoh Surat Izin Pegawai untuk pelaksana kerja','1651167980_instructions-for-adding-your-logo.pdf'),
(7,'2019-10-12 17:04:30',5,'MN-005','Contoh SPK Proyek Kontraktor','pdf',5,'Contoh SPK Proyek Kontraktor adalah contoh surat SPK KONTRAK','142845393_OoPdfFormExample.pdf'),
(8,'2019-10-12 17:05:22',5,'MN-006','SPK Kontrak Jembatan','pdf',6,'Surat SPK Kontrak Jembatan Layang','106615077_sample-link_1.pdf'),
(9,'2019-10-12 17:06:55',6,'MN-008','Contoh Curiculum Vitae Untuk Lamaran Kerja','pdf',10,'Contoh Curiculum Vitae Untuk Lamaran Kerja untuk pegawai baru','927990343_pdf-sample(1).pdf'),
(10,'2019-10-12 17:07:30',6,'MN-009','Surat Cuti Sakit Pegawai','pdf',7,'Contoh Surat Cuti Sakit Pegawai baru','2071946811_PEMBUATAN FILE PDF_FNH_tamim (1).pdf');

/*Table structure for table `disposisi` */

DROP TABLE IF EXISTS `disposisi`;

CREATE TABLE `disposisi` (
  `disposisi_id` int(11) NOT NULL AUTO_INCREMENT,
  `disposisi_tujuan` varchar(50) NOT NULL,
  `disposisi_isi` text NOT NULL,
  `disposisi_catatan` text NOT NULL,
  `id_surat` int(11) NOT NULL,
  `id_admin` int(11) NOT NULL,
  PRIMARY KEY (`disposisi_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

/*Data for the table `disposisi` */

insert  into `disposisi`(`disposisi_id`,`disposisi_tujuan`,`disposisi_isi`,`disposisi_catatan`,`id_surat`,`id_admin`) values 
(1,'Sekretariat','satu','dua',1,1),
(2,'SubBag Umum dan Kepegawaian','jfdks','jkfbg',1,1),
(3,'SubBag Umum dan Kepegawaian','vfdl','kvfm',1,1),
(4,'SubBag Umum dan Kepegawaian','fdg','fdg',2,1),
(5,'SubBag Penyusunan Program dan Keuangan','fedgr','grfsh',2,1),
(6,'SubBag Penyusunan Program dan Keuangan','fedgr','grfsh',2,1),
(7,'Perencanaan dan Pengendalian','fedgr','grfsh',2,1),
(8,'Perencanaan dan Pengendalian','jfdks','kd',2,1),
(9,'Perencanaan dan Pengendalian','saya','saya',2,1),
(10,'Perencanaan dan Pengendalian','saya','saya',2,1),
(11,'Perencanaan dan Pengendalian','saya','saya',2,1),
(12,'SubBag Umum dan Kepegawaian','124','325',2,1),
(13,'Sekretariat','fjdhlk','jghfjkhnjdfnhl',2,1),
(14,'Sekretariat','sa','gfhgjg',1,1);

/*Table structure for table `kategori` */

DROP TABLE IF EXISTS `kategori`;

CREATE TABLE `kategori` (
  `kategori_id` int(11) NOT NULL AUTO_INCREMENT,
  `kategori_nama` varchar(255) NOT NULL,
  `kategori_keterangan` text NOT NULL,
  PRIMARY KEY (`kategori_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/*Data for the table `kategori` */

insert  into `kategori`(`kategori_id`,`kategori_nama`,`kategori_keterangan`) values 
(1,'Tidak berkategori','Semua yang tidak memiliki kategori'),
(4,'Surat Izin Pelaksanaan','Contoh format surat izin pelaksaan pekerjaan'),
(5,'Surat Perintah Kerja Proyek jalan','Contoh format surat perintah untuk pekerjaan proyek jalan'),
(7,'Surat Kesehatan Pegawai','Surat kesehatan untuk pegawai'),
(8,'Surat Lampiran Skripsi','Surat contoh lampiran untuk skripsi'),
(10,'Curiculum Vitae','Contoh format surat curiculum vitae untuk kenaikan jabatan'),
(11,'Surat Masuk','kategori untuk surat masuk');

/*Table structure for table `klasifikasi` */

DROP TABLE IF EXISTS `klasifikasi`;

CREATE TABLE `klasifikasi` (
  `klasifikasi_id` int(11) NOT NULL AUTO_INCREMENT,
  `klasifikasi_kode` varchar(20) NOT NULL,
  `klasifikasi_nama` varchar(250) NOT NULL,
  `klasifikasi_uraian` text NOT NULL,
  PRIMARY KEY (`klasifikasi_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `klasifikasi` */

insert  into `klasifikasi`(`klasifikasi_id`,`klasifikasi_kode`,`klasifikasi_nama`,`klasifikasi_uraian`) values 
(1,'973','Pajak','Pajak'),
(4,'970','Pengadaan','pengadaan');

/*Table structure for table `petugas` */

DROP TABLE IF EXISTS `petugas`;

CREATE TABLE `petugas` (
  `petugas_id` int(11) NOT NULL AUTO_INCREMENT,
  `petugas_nama` varchar(255) NOT NULL,
  `petugas_nip` varchar(30) NOT NULL,
  `petugas_bagian` varchar(50) NOT NULL,
  `petugas_username` varchar(255) NOT NULL,
  `petugas_password` varchar(255) NOT NULL,
  `petugas_foto` varchar(255) NOT NULL,
  PRIMARY KEY (`petugas_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

/*Data for the table `petugas` */

insert  into `petugas`(`petugas_id`,`petugas_nama`,`petugas_nip`,`petugas_bagian`,`petugas_username`,`petugas_password`,`petugas_foto`) values 
(4,'Maria Helena','87654321','Pembukuan dan Penagihan','petugas1','b53fe7751b37e40ff34d012c7774d65f',''),
(5,'Junaidi Mus','12345678','SubBag Penyusunan Program dan Keuangan','petugas2','ac5604a8b8504d4ff5b842480df02e91',''),
(6,'Jamilah Suanda','673492','SubBag Umum dan Kepegawaian','petugas3','6f7dc121bccfd778744109cac9593474',''),
(7,'Sakina','12576','Pembukuan dan Penagihan','sakina','95c477e4932b3b16500674c18fb6f9a4','');

/*Table structure for table `riwayat` */

DROP TABLE IF EXISTS `riwayat`;

CREATE TABLE `riwayat` (
  `riwayat_id` int(11) NOT NULL AUTO_INCREMENT,
  `riwayat_waktu` datetime NOT NULL,
  `riwayat_user` int(11) NOT NULL,
  `riwayat_arsip` int(11) NOT NULL,
  PRIMARY KEY (`riwayat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Data for the table `riwayat` */

insert  into `riwayat`(`riwayat_id`,`riwayat_waktu`,`riwayat_user`,`riwayat_arsip`) values 
(1,'2019-10-11 15:32:29',8,1),
(2,'2019-10-12 17:09:31',8,10),
(3,'2019-10-12 17:09:45',8,9),
(4,'2019-10-12 17:09:50',8,8),
(5,'2019-10-12 17:09:53',8,3),
(6,'2019-10-12 17:10:07',9,10),
(7,'2019-10-12 17:10:16',9,9),
(8,'2019-10-12 17:10:19',9,8),
(9,'2019-10-12 17:10:22',9,6),
(10,'2019-10-12 17:10:26',9,2);

/*Table structure for table `surat_keluar` */

DROP TABLE IF EXISTS `surat_keluar`;

CREATE TABLE `surat_keluar` (
  `id_surat` int(11) NOT NULL AUTO_INCREMENT,
  `no_agenda` int(11) NOT NULL,
  `tujuan` varchar(250) NOT NULL,
  `no_surat` varchar(50) NOT NULL,
  `isi` text NOT NULL,
  `kode` varchar(20) NOT NULL,
  `tgl_surat` date NOT NULL,
  `tgl_catat` date NOT NULL,
  `file` varchar(100) NOT NULL,
  `jenis` varchar(20) NOT NULL,
  `keterangan` varchar(250) NOT NULL,
  `status` varchar(30) NOT NULL,
  `id_petugas` int(11) NOT NULL,
  PRIMARY KEY (`id_surat`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `surat_keluar` */

insert  into `surat_keluar`(`id_surat`,`no_agenda`,`tujuan`,`no_surat`,`isi`,`kode`,`tgl_surat`,`tgl_catat`,`file`,`jenis`,`keterangan`,`status`,`id_petugas`) values 
(5,859,'jfkf','7468439','cskj','mvfks','2020-10-04','2020-10-04','411504504_287-729-1-PB.pdf','pdf','kjnvfs','Dikirim',4),
(6,8495,'vsk','','vdjks','mskb','2020-10-04','2020-10-04','2042175887_287-729-1-PB.pdf','pdf','vfkjs','Tidak Dikirim',4);

/*Table structure for table `surat_masuk` */

DROP TABLE IF EXISTS `surat_masuk`;

CREATE TABLE `surat_masuk` (
  `id_surat` int(11) NOT NULL AUTO_INCREMENT,
  `no_agenda` int(11) NOT NULL,
  `asal_surat` varchar(250) NOT NULL,
  `no_surat` varchar(50) NOT NULL,
  `isi` text NOT NULL,
  `kode` varchar(20) NOT NULL,
  `indeks` varchar(100) NOT NULL,
  `tgl_surat` date NOT NULL,
  `tgl_catat` date NOT NULL,
  `file` varchar(100) NOT NULL,
  `jenis` varchar(20) NOT NULL,
  `keterangan` varchar(250) NOT NULL,
  `id_admin` int(11) NOT NULL,
  PRIMARY KEY (`id_surat`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `surat_masuk` */

insert  into `surat_masuk`(`id_surat`,`no_agenda`,`asal_surat`,`no_surat`,`isi`,`kode`,`indeks`,`tgl_surat`,`tgl_catat`,`file`,`jenis`,`keterangan`,`id_admin`) values 
(1,24,'PT Sumber Sari','abcde','menutup usaha dikarenakan pandemi covid','097','Pajak','2020-09-25','2020-09-30','1203674873_artikel_jmasif_okt_11-Panji.pdf','pdf','surat masuk dari PT Sumber Sari',1),
(2,0,'xca','dfdfd','fdf','23','fdfd','2020-10-06','2020-10-06','204509524_1.jpg','jpg','tery5etuh',1);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
