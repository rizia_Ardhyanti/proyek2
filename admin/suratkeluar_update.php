<?php 
include '../koneksi.php';
session_start();
date_default_timezone_set('Asia/Jakarta');

// $waktu = date('Y-m-d H:i:s'); 
// $petugas = $_SESSION['id'];
$id         = $_POST['id'];
$no_agenda  = $_POST['no_agenda'];
$tujuan     = $_POST['tujuan'];
$no_surat   = $_POST['no_surat'];
$isi        = $_POST['isi'];
$kode       = $_POST['kode'];
$tgl_surat  = $_POST['tgl_surat']; 
$keterangan = $_POST['keterangan'];
$status = $_POST['status'];

$rand = rand();

$filename = $_FILES['file']['name'];

if($filename == ""){

    mysqli_query($koneksi, "update surat_keluar set no_agenda='$no_agenda', tujuan='$tujuan', no_surat='$no_surat', isi='$isi', kode='$kode', tgl_surat='$tgl_surat', keterangan='$keterangan', status='$status' where id_surat='$id'")or die(mysqli_error($koneksi));
    header("location:suratkeluar.php");

}else{

    $jenis = pathinfo($filename, PATHINFO_EXTENSION);

    if($jenis == "php") {
        header("location:suratkeluar.php?alert=gagal");
    }else{
        // hapus file lama
        $lama = mysqli_query($koneksi,"select * from surat_keluar where id_surat='$id'");
        $l = mysqli_fetch_assoc($lama);
        $nama_file_lama = $l['file'];
        unlink("../arsip/".$nama_file_lama);

        // upload file baru
        move_uploaded_file($_FILES['file']['tmp_name'], '../arsip/'.$rand.'_'.$filename);
        $nama_file = $rand.'_'.$filename;
        mysqli_query($koneksi, "update surat_keluar set no_agenda='$no_agenda', tujuan='$tujuan', no_surat='$no_surat', isi='$isi', kode='$kode', tgl_surat='$tgl_surat', file='$nama_file', jenis='$jenis', keterangan='$keterangan', status='$status' where id_surat='$id'")or die(mysqli_error($koneksi));
        header("location:suratkeluar.php?alert=sukses");
    }
}

