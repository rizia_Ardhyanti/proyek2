<?php 
include '../koneksi.php';
session_start();
date_default_timezone_set('Asia/Jakarta');

// $waktu = date('Y-m-d H:i:s'); 
// $petugas = $_SESSION['id'];
$id         = $_POST['id'];
$no_agenda  = $_POST['no_agenda'];
$asal_surat = $_POST['asal_surat'];
$no_surat   = $_POST['no_surat'];
$isi        = $_POST['isi'];
$kode       = $_POST['kode'];
$indeks     = $_POST['indeks'];
$tgl_surat  = $_POST['tgl_surat']; 
$keterangan = $_POST['keterangan'];

$rand = rand();

$filename = $_FILES['file']['name'];

if($filename == ""){

    mysqli_query($koneksi, "update surat_masuk set no_agenda='$no_agenda', asal_surat='$asal_surat', no_surat='$no_surat', isi='$isi', kode='$kode', indeks='$indeks', tgl_surat='$tgl_surat', keterangan='$keterangan' where id_surat='$id'")or die(mysqli_error($koneksi));
    header("location:suratmasuk.php");

}else{

    $jenis = pathinfo($filename, PATHINFO_EXTENSION);

    if($jenis == "php") {
        header("location:suratmasuk.php?alert=gagal");
    }else{
        // hapus file lama
        $lama = mysqli_query($koneksi,"select * from surat_masuk where id_surat='$id'");
        $l = mysqli_fetch_assoc($lama);
        $nama_file_lama = $l['file'];
        unlink("../arsip/".$nama_file_lama);

        // upload file baru
        move_uploaded_file($_FILES['file']['tmp_name'], '../arsip/'.$rand.'_'.$filename);
        $nama_file = $rand.'_'.$filename;
        mysqli_query($koneksi, "update surat_masuk set no_agenda='$no_agenda', asal_surat='$asal_surat', no_surat='$no_surat', isi='$isi', kode='$kode', indeks='$indeks', tgl_surat='$tgl_surat', file='$nama_file', jenis='$jenis', keterangan='$keterangan' where id_surat='$id'")or die(mysqli_error($koneksi));
        header("location:suratmasuk.php?alert=sukses");
    }
}

