<?php include 'header.php'; ?>

<div class="breadcome-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcome-list">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="breadcome-heading">
                                <h4 style="margin-bottom: 0px">Disposisi</h4>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <ul class="breadcome-menu" style="padding-top: 0px">
                                <li><a href="#">Home</a> <span class="bread-slash">/</span></li>
                                <li><span class="bread-blod">Disposisi</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Form tambah -->

<div class="container-fluid">


    <div class="row">
        <div class="col-lg-6">
            <div class="panel panel">

                <div class="panel-heading">
                    <h3 class="panel-title">Tambah Disposisi</h3>
                </div>
                <div class="panel-body">

                    <div class="pull-right">            
                        <a href="suratmasuk.php" class="btn btn-sm btn-primary"><i class="fa fa-arrow-left"></i> Kembali</a>
                    </div>

                    <br>
                    <br>

                    <?php 
                    $id = $_GET['id'];              
                    $data = mysqli_query($koneksi, "select * from surat_masuk where id_surat='$id'");
                    while($d = mysqli_fetch_array($data)){
                        ?>

                    <form method="post" action="disposisi_aksi.php" enctype="multipart/form-data">
                        <input type="hidden" name="id" value="<?php echo $d['id_surat']; ?>">

                        <div class="form-group">
                            <label>Tujuan</label>
                            <select class="form-control" id="tujuan" name="tujuan" style="width: 100%;">
                                <option value=" ">-- Pilih Tujuan --</option>
                                <option>Sekretariat</option>
                                <option>SubBag Umum dan Kepegawaian</option>
                                <option>SubBag Penyusunan Program dan Keuangan</option>
                                <option>Perencanaan dan Pengendalian</option>
                                <option>Pendataan dan Penetapan</option>
                                <option>Pembukuan dan Penagihan</option>
                              </select>
                        </div>

                        <div class="form-group">
                            <label>Isi</label>
                            <input type="text" class="form-control" name="isi" required="required">
                        </div>

                        <div class="form-group">
                            <label>Catatan</label>
                            <input type="text" class="form-control" name="catatan" required="required">
                        </div>


                        <div class="form-group">
                            <label></label>
                            <input type="submit" class="btn btn-primary" value="Simpan">
                        </div>

                    </form>
                    
                    <?php 
                    }
                    ?>

                </div>
            </div>
        </div>
    </div>


</div>

<!--End Form tambah -->

<!--Tampil disposisi -->

<div class="container-fluid">
    <div class="panel panel">

        <div class="panel-heading">
            <h3 class="panel-title">Disposisi</h3>
        </div>
        <div class="panel-body">

            <table id="table" class="table table-bordered table-striped table-hover table-datatable">
                <thead>
                    <tr>
                        <th width="1%">No</th>
                        <th>Tujuan</th>
                        <th>Isi</th>
                        <th>Catatan</th>
                        <th class="text-center" width="10%">Tindakan</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                    include '../koneksi.php';
                    $key= 1;
                    $disposisi = mysqli_query($koneksi, "SELECT * FROM disposisi where id_surat='$id'");
                    $jumlah = mysqli_num_rows($disposisi);
                    while($p = mysqli_fetch_array($disposisi)){
                        ?>
                        <tr>
                            <td><?php echo $key++  ?></td>
                            <td><?php echo $p['disposisi_tujuan'] ?></td>
                            <td><?php echo $p['disposisi_isi'] ?></td>
                            <td><?php echo $p['disposisi_catatan'] ?></td>
                            <td class="text-center">

                                    <div class="modal fade" id="exampleModal_<?php echo $p['disposisi_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">PERINGATAN!</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                Apakah anda yakin ingin menghapus data ini? <br>file dan semua yang berhubungan akan dihapus secara permanen.
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batalkan</button>
                                                <a href="disposisi_hapus.php?id=<?php echo $p['disposisi_id'];?>&id_surat=<?= $p['id_surat'];  ?>" class="btn btn-primary"><i class="fa fa-check"></i> &nbsp; Ya, hapus</a>
                                            </div>
                                        </div>
                                    </div>
                                    </div>

                                    <div class="btn-group">
                                        <a href="disposisi_edit.php?id=<?php echo $p['disposisi_id']; ?>" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                                        <a href="" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal_<?php echo $p['disposisi_id']; ?>"><i class="fa fa-trash"></i></a>
                                    </div>
 
                            </td>
                        </tr>
                        <?php 
                    }
                    ?>
                </tbody>
            </table>


        </div>

    </div>
</div>


<?php include 'footer.php'; ?>