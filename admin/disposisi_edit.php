
<?php include 'header.php'; ?>

<div class="breadcome-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcome-list">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="breadcome-heading">
                                <h4 style="margin-bottom: 0px">Edit Disposisi</h4>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <ul class="breadcome-menu" style="padding-top: 0px">
                                <li><a href="#">Home</a> <span class="bread-slash">/</span></li>
                                <li><span class="bread-blod">Disposisi</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">


    <div class="row">
        <div class="col-lg-6">
            <div class="panel panel">

                <div class="panel-heading">
                    <h3 class="panel-title">Edit Disposisi</h3>
                </div>
                <div class="panel-body">

                    <div class="pull-right">            
                        <a href="suratmasuk.php" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Kembali</a>
                    </div>
                    <br>
                    <br>

                    <?php 
                    $id = $_GET['id'];              
                    $data = mysqli_query($koneksi, "select * from disposisi where disposisi_id='$id'");
                    while($d = mysqli_fetch_array($data)){
                        ?>

                        <form method="post" action="disposisi_update.php">

                                <input type="hidden" name="id_surat" value="<?php echo $d['id_surat']; ?>">
                                <input type="text" name="disposisi_id" value="<?php echo $d['disposisi_id']; ?>">


                            <div class="form-group">
                                <label>Tujuan</label>
                                <select class="form-control" id="disposisi_tujuan" name="disposisi_tujuan" style="width: 100%;">
                                    <option value="Sekretariat" <?php $d['disposisi_tujuan'] == 'Sekretariat' ? print "selected" : ""; ?>>Sekretariat</option>
                                    <option value="SubBag Umum dan Kepegawaian"<?php $d['disposisi_tujuan'] == 'SubBag Umum dan Kepegawaian' ? print "selected" : ""; ?>>SubBag Umum dan Kepegawaian</option>
                                    <option value="SubBag Penyusunan Program dan Keuangan"<?php $d['disposisi_tujuan'] == 'SubBag Penyusunan Program dan Keuangan' ? print "selected" : ""; ?>>SubBag Penyusunan Program dan Keuangan</option>
                                    <option value="Perencanaan dan Pengendalian"<?php $d['disposisi_tujuan'] == 'Perencanaan dan Pengendalian' ? print "selected" : ""; ?>>Perencanaan dan Pengendalian</option>
                                    <option value="Pendataan dan Penetapan"<?php $d['disposisi_tujuan'] == 'Pendataan dan Penetapan' ? print "selected" : ""; ?>>Pendataan dan Penetapan</option>
                                    <option value="Pembukuan dan Penagihan"<?php $d['disposisi_tujuan'] == 'Pembukuan dan Penagihan' ? print "selected" : ""; ?>>Pembukuan dan Penagihan</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Isi</label>
                                <textarea class="form-control" name="disposisi_isi" required="required"><?php echo $d['disposisi_isi']; ?></textarea>
                            </div>

                            <div class="form-group">
                                <label>Catatan</label>
                                <textarea class="form-control" name="disposisi_catatan" required="required"><?php echo $d['disposisi_catatan']; ?></textarea>
                            </div>

                            <div class="form-group">
                                <label></label>
                                <input type="submit" class="btn btn-primary" value="Simpan">
                            </div>

                        </form>

                        <?php 
                    }
                    ?>

                </div>
            </div>
        </div>
    </div>


</div>


<?php include 'footer.php'; ?>

